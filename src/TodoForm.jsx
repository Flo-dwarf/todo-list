import React, { Component } from "react";

class TodoForm extends Component {
  state = {
    nouvelleTache: "",
  };

  handleChange = (event) => {
    this.setState({ nouvelleTache: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const id = new Date().getTime();
    const todo = this.state.nouvelleTache;

    this.props.onTodoAdd({ id, todo });
    this.setState({ nouvelleTache: "" });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          value={this.state.nouvelleTache}
          onChange={this.handleChange}
          type="text"
          placeholder="Ajouter une tache"
        />
        <button>Confirmer</button>
      </form>
    );
  }
}

export default TodoForm;