import React from "react";
import "./App.css";
import Todo from "./Todo";
import TodoForm from "./TodoForm";

class App extends React.Component {
  state = {
    todos: [
      { id: 1, todo: "Bloup", enabled: false },
      { id: 2, todo: "Pouit", enabled: false },
      { id: 3, todo: "Kiki", enabled: false },
    ],
  };

  handleDelete = (id) => {
    const todos = [...this.state.todos]; // = this.state.todos.slice();
    const index = todos.findIndex((todo) => todo.id === id);

    todos.splice(index, 1);

    this.setState({ todos: todos });
  };

  handleAdd = (todo) => {
    const todos = [...this.state.todos]; // = this.state.todos.slice();
    todos.push(todo);

    this.setState({ todos });
  };

  render() {
    return (
      <div className="App">
        <h1>TODOS</h1>
        <ul>
          {this.state.todos.map((todo) => (
            <Todo details={todo} onDelete={this.handleDelete} />
          ))}
        </ul>
        <TodoForm onTodoAdd={this.handleAdd} />
      </div>
    );
  }
}

export default App;
