import React from "react";

const Todo = ({ details, onDelete }) => (
  <li>
    {details.todo}
    <input type="checkbox" value={details.todo} />
    <button onClick={() => onDelete(details.id)}>X</button>
  </li>
);
export default Todo;
